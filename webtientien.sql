-- MySQL dump 10.13  Distrib 5.7.24, for Linux (x86_64)
--
-- Host: localhost    Database: webtientien
-- ------------------------------------------------------
-- Server version	5.7.24-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `activite`
--

DROP TABLE IF EXISTS `activite`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `activite` (
  `id` int(255) NOT NULL,
  `name` varchar(20) NOT NULL,
  `max_score` float NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activite`
--

LOCK TABLES `activite` WRITE;
/*!40000 ALTER TABLE `activite` DISABLE KEYS */;
INSERT INTO `activite` VALUES (1,'activite_1',27);
/*!40000 ALTER TABLE `activite` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer`
--

DROP TABLE IF EXISTS `customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer` (
  `id` int(255) NOT NULL,
  `name` varchar(30) NOT NULL,
  `email` varchar(30) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer`
--

LOCK TABLES `customer` WRITE;
/*!40000 ALTER TABLE `customer` DISABLE KEYS */;
/*!40000 ALTER TABLE `customer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_score`
--

DROP TABLE IF EXISTS `customer_score`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer_score` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `customer_id` int(255) NOT NULL,
  `activite_id` int(255) NOT NULL,
  `activite_name` varchar(255) NOT NULL,
  `scores` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_score`
--

LOCK TABLES `customer_score` WRITE;
/*!40000 ALTER TABLE `customer_score` DISABLE KEYS */;
INSERT INTO `customer_score` VALUES (1,1,10,'activite_10','9.71'),(2,1,10,'activite_10','8.86'),(3,1,10,'activite_10','5.43'),(4,1,10,'activite_10','9.71'),(5,1,10,'activite_10','5.43'),(6,1,10,'activite_10','4.29'),(7,1,10,'activite_10','9.71'),(8,1,10,'activite_10','9.71'),(9,1,10,'activite_10','9.71'),(10,1,10,'activite_10','4'),(11,1,10,'activite_10','7.14'),(12,1,10,'activite_10','9.71'),(13,1,10,'activite_10','5.43'),(14,1,10,'activite_10','4'),(15,1,10,'activite_10','9.71'),(16,1,10,'activite_10','6'),(17,1,5,'activite_5','9.35'),(18,1,5,'activite_5','7.42'),(19,1,5,'activite_5','6.13'),(20,1,5,'activite_5','6.13'),(21,1,5,'activite_5','5.16'),(22,1,5,'activite_5','2.26'),(23,1,5,'activite_5','5.81'),(24,1,5,'activite_5','7.74'),(25,1,5,'activite_5','9.35'),(26,1,5,'activite_5','9.35'),(27,1,5,'activite_5','5.48');
/*!40000 ALTER TABLE `customer_score` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lessons`
--

DROP TABLE IF EXISTS `lessons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lessons` (
  `id` int(255) NOT NULL,
  `lesson_name` varchar(255) NOT NULL,
  `content` varchar(2000) NOT NULL,
  `max_score` float DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lessons`
--

LOCK TABLES `lessons` WRITE;
/*!40000 ALTER TABLE `lessons` DISABLE KEYS */;
INSERT INTO `lessons` VALUES (1,'activite 1','Départ du train 2365 à destination de Rennes à 13 h 50 voie B. Les voies A à D se situent au premier étage par l\'escalier roulant',27),(2,'activite 2','Chers clients, aujourd\'hui, le magasin reste ouvert jusqu\'à 21h. Pour vous éviter une attente trop longue aux caisses du rez-de-chaussée, vous pouvez régler vos achats au 1er étage, des caisses supplémentaires sont à votre disposition',NULL),(3,'activite 3','Demain mercredi, la piscine fermera ses portes à\r\n20h en raison d\'une compétition régionale',NULL),(4,'activite 4','Chers spectateurs, vos téléphones portables doivent être éteints pendant toute la durée de la représentation. Merci de votre compréhension',NULL),(5,'activite 5','Suite à une erreur informatique, votre commande n\'a pas été enregistrée. Nous vous demandons de nous rappeler avant dix heures demain au numéro gratuit 0856439131 pour nous donner votre adresse électronique',NULL),(6,'activite 6','Tu n\'as pas oublié que samedi on fêtait mon arrivée dans mon nouvel appartement ? La station de métro la plus proche est République et surtout n\'oublie pas le code de la porte d\'entrée 452T. À samedi alors ',NULL),(7,'activite 7','Madame Deval ? C\'est Arthur Culvy, le professeur de guitare de votre fils. Je ne pourrai pas venir cet après-midi mais demain après le lycée, Léo peut rattraper le cours avec sa cousine Isabelle et Martin.',NULL),(8,'activite 8','Mais arrêtez de pousser ! J\'étais avant vous dans la file. Attendez votre tour comme tout le monde. Non mais ce n\'est pas possible ',NULL),(9,'activite 9','Comme Guillaume, d\'autres jeunes attendent beaucoup de ce rendez-vous du salon de l\'innovation et de l\'entreprise. Eux ont créé un boîtier intelligent qui diffuse des morceaux de musique sans faire de téléchargement avant',NULL),(10,'activite 10','Pour jouer à notre jeu et partager avec le candidat la somme de 1000 euros, envoyez un texto avant 14h au 3820 en tapant 1 pour la réponse A et 2 pour la réponse B.',NULL);
/*!40000 ALTER TABLE `lessons` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_password`
--

DROP TABLE IF EXISTS `user_password`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_password` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `userName` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `type_customer` int(11) NOT NULL COMMENT '0-Admin, 1-Customer',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_password`
--

LOCK TABLES `user_password` WRITE;
/*!40000 ALTER TABLE `user_password` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_password` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-12-08 16:18:36
