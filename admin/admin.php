<?php
// Start the session
session_start();
?>
<!DOCTYPE html>
<html>
<head><title>Admin</title>
    <style>#loader {
            transition: all .3s ease-in-out;
            opacity: 1;
            visibility: visible;
            position: fixed;
            height: 100vh;
            width: 100%;
            background: #fff;
            z-index: 90000
        }

        #loader.fadeOut {
            opacity: 0;
            visibility: hidden
        }

        .spinner {
            width: 40px;
            height: 40px;
            position: absolute;
            top: calc(50% - 20px);
            left: calc(50% - 20px);
            background-color: #333;
            border-radius: 100%;
            -webkit-animation: sk-scaleout 1s infinite ease-in-out;
            animation: sk-scaleout 1s infinite ease-in-out
        }

        @-webkit-keyframes sk-scaleout {
            0% {
                -webkit-transform: scale(0)
            }
            100% {
                -webkit-transform: scale(1);
                opacity: 0
            }
        }

        @keyframes sk-scaleout {
            0% {
                -webkit-transform: scale(0);
                transform: scale(0)
            }
            100% {
                -webkit-transform: scale(1);
                transform: scale(1);
                opacity: 0
            }
        }</style>
    <link href="./css/style.css" rel="stylesheet">
    <link href="./css/admin.css" rel="stylesheet">
    <link rel="icon" href="../img/favicon.png" type="image/x-ico"/>
</head>
<body class="app">
<div id="loader">
    <div class="spinner"></div>
</div>
<script type="text/javascript">window.addEventListener('load', () => {
        const loader = document.getElementById('loader');
        setTimeout(() => {
            loader.classList.add('fadeOut');
        }, 300);
    });</script>
<div>
    <div class="sidebar">
        <div class="sidebar-inner">
            <div class="sidebar-logo">
                <div class="peers ai-c fxw-nw">
                    <div class="peer peer-greed"><a class="sidebar-link td-n" class="td-n">
                            <div class="peers ai-c fxw-nw">
                                <div class="peer">
                                    <div class="logo"></div>
                                </div>
                                <div class="peer peer-greed"><h5 class="lh-1 mB-0 logo-text">Adminator</h5></div>
                            </div>
                        </a></div>
                    <div class="peer">
                        <div class="mobile-toggle sidebar-toggle"><a class="td-n"><i
                                        class="ti-arrow-circle-left"></i></a></div>
                    </div>
                </div>
            </div>
            <ul class="sidebar-menu scrollable pos-r">
                <li id="manage-exercise" class="nav-item mT-30" style="cursor: pointer;"><a class="sidebar-link" "
                    default><span
                            class="icon-holder"><i class="c-blue-500 ti-home"></i> </span><span
                            class="title">All Exercise</span></a></li>
                <li id="add-exercise" class="nav-item" style="cursor: pointer;"><a class="sidebar-link"><span
                                class="icon-holder"><i class="c-orange-500 ti-layout-list-thumb"></i> </span><span
                                class="title">Add Exercise</span></a>
                </li>
            </ul>
        </div>
    </div>
    <div class="page-container">
        <div class="header navbar">
            <div class="header-container">
                <ul class="nav-left">
                    <li><a id="sidebar-toggle" class="sidebar-toggle" href="javascript:void(0);"><i class="ti-menu"></i></a>
                    </li>
                    <li class="search-input"><input class="form-control" type="text" placeholder="Search..."></li>
                </ul>
                <ul class="nav-right">
                    <li class="notifications dropdown"><a class="dropdown-toggle no-after">
                            <i class="ti-bell"></i></a>
                    </li>
                    <li class="notifications dropdown">
                        <a class="dropdown-toggle no-after">
                            <i class="ti-email"></i></a>
                    </li>
                    <li class="username"><a style="cursor: pointer"
                                            class="dropdown-toggle no-after peers fxw-nw ai-c lh-1">
                            <div class="peer"><span id="username" class="fsz-sm c-grey-900"></span></div>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <main class="main-content bgc-grey-100">
            <div id="mainContent-1" style="display: block">
                <h1 style="text-align:center">Manage Exercise</h1>
                <?php
                header('Content-type: text/plain; charset= UTF-8');
                /*** mysql hostname ***/
                $hostname = 'localhost';

                /*** mysql username ***/
                $username = 'root';

                /*** mysql password ***/
                $password = '28071997';

                /*** mysql name database ***/
                $dbname = "webNC";

                $conn = new mysqli($hostname, $username, $password, $dbname);
                /*** echo a message saying we have connected ***/
                //    echo 'Connected to database';
                if ($conn->connect_error) {
                    die("Connection failed: " . $conn->connect_error);
                }
                $sql = "SELECT * FROM `exercise`";

                $data = $conn->query($sql);
                //                $row = mysqli_fetch_array($data, MYSQLI_ASSOC);
                ?>
                <?php if ($data->num_rows > 0) : ?>
                <table>
                    <tr>
                        <th style="width: 10%;text-align: center">ID</th>
                        <th style="width: 20%;text-align: center">Title</th>
                        <th style="width: 35%;text-align: center"
                        ">Answer</th>
                        <th style="text-align: center"
                        ">Description</th>
                        <th style="text-align: center"
                        ">Action</th>

                    </tr>
                    <?php $total = 0;
                    while ($row = $data->fetch_assoc()) { ?>
                        <tr>
                            <td style="text-align: center"><?php echo ++$total; ?></td>
                            <td style="text-align: center"><?php echo $row['title'] ?></td>
                            <td style="text-align: center"><?php echo $row['answer'] ?></td>
                            <td style="text-align: center"><?php echo $row['description'] ?></td>
                            <td class="action" style="text-align: center; width: 8%;">
                                <input id=<?php echo "checkbox-" . $row['id'] ?>  type="checkbox">
                                <label id=<?php echo $row['id'] ?> class="label-select" for="list-item-2">Select</label>
                                <ul id=<?php echo "ul-" . $row['id'] ?>>
                                    <li id=<?php echo "edit-" . $row['id'] ?>>Edit</li>
                                    <li id=<?php echo "delete-" . $row['id'] ?>>Delete</li>
                                </ul>
                            </td>
                        </tr>
                    <?php } ?>
                    <?php endif; ?>
                </table>
                <div>

                </div>
            </div>


            <!--            content add new exercise-->
            <div id="mainContent-2" style="display: none">
                <h1 style="text-align:center">Add Exercise</h1>
                <hr>
                <button id="submit-add-exercise" name="submit" type="submit" form="form-add-exercise" value="Submit">
                    Submit
                </button>
                <form id="form-add-exercise" action="../controller/upload.php" method="post"
                      enctype="multipart/form-data" target="_self">
                    <input name="current-url" value="" type="text" id="current-url" hidden>
                    <table>
                        <tbody>
                        <tr>
                            <td class="row-title">Title</td>
                            <td class="row-add" style="width: 300px;">
                                <input name="title" type="text"></td>
                        </tr>
                        <tr>
                            <td class="row-title">Choose File</td>
                            <td class="row-add">
                                Select audio to upload:
                                <input type="file" name="fileToUpload" id="fileToUpload">
                            </td>
                        </tr>
                        <tr>
                            <td class="row-title">Answer</td>
                            <td class="row-add">
                            <textarea name="answer" rows="6" cols="50">

                            </textarea>
                            </td>
                        </tr>
                        <tr>
                            <td class="row-title">Description</td>
                            <td class="row-add"><textarea name="description" rows="6" cols="50">

                            </textarea>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </form>
            </div>


<!--            edit exercise -->
            <div id="mainContent-3" style="display: none">
                <h1 style="text-align:center">Edit Exercise</h1>
                <hr>
                <button id="submit-add-exercise" name="submit" type="submit" form="form-edit-exercise" value="Submit">
                    Submit
                </button>
                <form id="form-edit-exercise" action="../controller/update.php" method="post"
                      enctype="multipart/form-data" target="_self">
                    <input name="current-url-edit" value="" type="text" id="current-url-edit" hidden>
                    <input name="id-edit" value="" type="text" id="id-edit" hidden>
                    <table>
                        <tbody>
                        <tr>
                            <td class="row-title">Title</td>
                            <td class="row-add" style="width: 300px;">
                                <input id="title-edit" name="title" type="text"></td>
                        </tr>
                        <tr>
                            <td class="row-title">Chose File</td>
                            <td class="row-add">
                                Select audio to upload:
                                <input type="file" name="fileToUpload" id="fileToUpload" style="width: 95px">
                                <label id="label-file" for="files"></label>
                            </td>
                        </tr>
                        <tr>
                            <td class="row-title">Answer</td>
                            <td class="row-add">
                            <textarea id="answer-edit" name="answer" rows="6" cols="50">

                            </textarea>
                            </td>
                        </tr>
                        <tr>
                            <td class="row-title">Description</td>
                            <td class="row-add"><textarea id="description-edit" name="description" rows="6" cols="50">

                            </textarea>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </form>
            </div>
        </main>
        <footer class="bdT ta-c p-30 lh-0 fsz-sm c-grey-600"><span>Designed by <a
                        target="_blank" title="Colorlib">Hoang Hong Ha</a>. All rights reserved.</span>
        </footer>
    </div>
</div>
<script type="text/javascript" src="vendor.js"></script>
<script type="text/javascript" src="bundle.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.2/rollups/aes.js"></script>

<!--js for tab all exercise-->
<script>
    $("#manage-exercise").css("background", "#ededed");
    $("#manage-exercise").click(function () {
        $("#mainContent-1").css("display", "block");
        $("#mainContent-2").css("display", "none");
        $("#mainContent-3").css("display", "none");
        $("#manage-exercise").css("background", "#ededed");
        $("#add-exercise").css("background", "#ffffff");

    });

    var clickMenu = 0;
    $(".ti-menu").click(function () {
        if (clickMenu === 0) {
            $("body").addClass(" is-collapsed");
            clickMenu = 1;
        } else {
            $("body").removeClass(" is-collapsed");
            clickMenu = 0;
        }
    });
    if (window.location.search !== "") {
        var dataStringURL = window.location.search.split("&");
        var usernameDecrypted = CryptoJS.AES.decrypt(decodeURIComponent(dataStringURL[0].replace("?username=", "")), "code secret").toString(CryptoJS.enc.Utf8);
        $("#username").html(usernameDecrypted);
    }
    $(".action").click(function (i) {
        var idUl = "#ul-" + i.target.id;
        var idCheckBox = "#checkbox-" + i.target.id;
        var idlabel = "#" + i.target.id;
        if (!$(idCheckBox).is(":checked")) {
            $(idCheckBox).prop('checked', true);
            $(idUl).css({
                "height": "100%",
                "transform-origin": "top",
                "transition": "transform .25s ease-out",
                "transform": "scaleY(1)"
            });
            $(idlabel).css({
                "color": "#41bdea",
                "font-weight": "bold"
            });
        } else {
            $(idCheckBox).prop('checked', false);
            $(idUl).css({
                "height": "0",
                "transition": "transform .2s ",
                "transform": "scaleY(0)"
            });
            $(idlabel).css({
                "color": "",
                "font-weight": ""
            });
        }
    });

    //when click delete exercise
    $(".action li").click(function (i) {
        $("#manage-exercise").css("background", "#ffffff");
        var id = i.target.id;
        var arrStr = id.split("-");
        if (arrStr[0] === "delete") {
            if (confirm("Are you sure you want to delete this?")) {
                $.ajax({
                    type: "POST",
                    url: '../controller/DeleteExercise.php',
                    data: {
                        id: arrStr[1],
                    },
                    success: function (data) {
                        if (data === "done") {
                            location.reload();
                        } else {
                            alert("Something wrong when delete this exercise");
                        }
                    }
                });
            }
            else {
                return false;
            }
        }
        if (arrStr[0] === "edit") {
            if (confirm("Are you sure you want to edit this?")) {
                $.ajax({
                    type: "POST",
                    url: '../controller/EditExercise.php',
                    data: {
                        id: arrStr[1],
                    },
                    success: function (data) {
                        if (data !== "error") {
                            var dataConvert = $.parseJSON(data);
                            console.log(dataConvert);
                            $("#mainContent-1").css("display", "none");
                            $("#mainContent-2").css("display", "none");
                            $("#mainContent-3").css("display", "block");
                            $("#title-edit").val(dataConvert['title']);
                            $("#answer-edit").val(dataConvert['answer']);
                            $("#description-edit").val(dataConvert['description']);
                            $('#label-file').text(dataConvert['file'].replace("../audio/", ""));
                            $("#current-url-edit").val(window.location.search);
                            $("#id-edit").val(dataConvert['id']);
                        }
                    }
                });
            }
            else {
                return false;
            }
        }

    });

</script>


<!--js for tab add new exersice-->
<script>
    $("#add-exercise").click(function () {
        $("#current-url").val(window.location.search);
        $("#mainContent-1").css("display", "none");
        $("#mainContent-3").css("display", "none");
        $("#mainContent-2").css("display", "block");
        $("#add-exercise").css("background", "#ededed");
        $("#manage-exercise").css("background", "#ffffff");
    });
</script>
</body>
</html>
