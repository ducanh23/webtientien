<?php
/**
 * Created by PhpStorm.
 * User: ducanh
 * Date: 19/10/2018
 * Time: 11:42
 */
include_once '../database/db.inc.php';
$lessonName = $_POST['lesson_name'];
$activiteId = $_POST['activite_id'];
$customerId = $_POST['customer_id'];
$s = array();
$maxScore = array();
$query = "Select * from lessons where lesson_name =" . " '" . $lessonName . " '";
$result = $conn->query($query);
$result = $result->fetch_assoc();
$lesson = utf8_encode($result['content']);
$lesson = strtolower($lesson);
$answer = (explode(" ", $lesson));

if (isset ($_POST['inputText'])) {
    $task = $_POST['inputText'];
    $task = trim($task);
    $task = strtolower($task);
//    $task = trim(preg_replace('/\s\s+\n/', ' ', $task));
    $task = str_replace("\n", " ", $task);
    $task = (explode(" ", $task));
    if (substr(end($task), -1) == ".") {
        rtrim($task[24],". ");
    }
    for ($i = 0; $i < sizeof($task); $i++) {
        for ($j = 0; $j < sizeof($answer); $j++) {

            if ($task[0] == $answer[0]) {
                $s[0][0] = 1;
            } else {
                $s[0][0] = 0;
            }

            if ($i == 0 && $j > 0) {
                if ($task[$i] == $answer[$j] || $s[$i][$j - 1] == 1) {
                    $s[$i][$j] = 1;
                } else {
                    $s[$i][$j] = 0;
                }
            }

            if ($i > 0 && $j == 0) {
                if ($task[$i] == $answer[$j] || $s[$i - 1][$j] == 1) {
                    $s[$i][$j] = 1;
                } else {
                    $s[$i][$j] = 0;
                }
            }

            if ($i != 0 && $j != 0) {
                if ($task[$i] == $answer[$j]) {
                    $s[$i][$j] = $s[$i - 1][$j - 1] + 1;
                } else {
                    $s[$i][$j] = max($s[$i - 1][$j], $s[$i][$j - 1]);
                }
            }
        }
    }

//     MAX SCORE
    for ($i = 0; $i < sizeof($answer); $i++) {
        for ($j = 0; $j < sizeof($answer); $j++) {

            if ($answer[0] == $answer[0]) {
                $maxScore[0][0] = 1;
            } else {
                $maxScore[0][0] = 0;
            }

            if ($i == 0 && $j > 0) {
                if ($answer[$i] == $answer[$j] || $maxScore[$i][$j - 1] == 1) {
                    $maxScore[$i][$j] = 1;
                } else {
                    $maxScore[$i][$j] = 0;
                }
            }

            if ($i > 0 && $j == 0) {
                if ($answer[$i] == $answer[$j] || $maxScore[$i - 1][$j] == 1) {
                    $maxScore[$i][$j] = 1;
                } else {
                    $maxScore[$i][$j] = 0;
                }
            }

            if ($i != 0 && $j != 0) {
                if ($answer[$i] == $answer[$j]) {
                    $maxScore[$i][$j] = $maxScore[$i - 1][$j - 1] + 1;
                } else {
                    $maxScore[$i][$j] = max($maxScore[$i - 1][$j], $maxScore[$i][$j - 1]);
                }
            }
        }
    }
    $a = $s[sizeof($task) - 1][sizeof($answer) - 1];
    $b = $maxScore[sizeof($answer) - 1][sizeof($answer) - 1];
    $rateOn10 = ($a * 10) / $b;
    $rateOn10 = round($rateOn10, 2);
    $lessonName = str_replace(' ', '_', $lessonName);
    $query = "Insert into customer_score (customer_id,activite_id,activite_name,scores) values ("
        ."'".$customerId."'".","
        ."'".$activiteId."'".","
        ."'".$lessonName."'".","
        ."'".$rateOn10."'".")";
    $result = $conn->query($query);
//    echo $a;
    echo $rateOn10;
}