<?php
/**
 * Created by PhpStorm.
 * User: ducanh
 * Date: 04/12/2018
 * Time: 19:28
 */
include_once '../database/db.inc.php';
$lessons = [];
$query = "Select lesson_name,content from lessons ";
$result = $conn->query($query);
$numberOfRound = $result->num_rows;

if ($numberOfRound > 0) {
    while($row = $result->fetch_assoc()) {
        $row['content'] = utf8_encode($row['content']);
        array_push($lessons,$row['lesson_name']);
        array_push($lessons,$row['content']);
    }
}

$lessons = json_encode($lessons);

echo $lessons;