<?php
include_once '../database/db.inc.php';

$target_dir = "../audio/";
if (isset($_POST)) {
    if($_FILES["fileToUpload"]["name"]!="") {
        $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
        $uploadOk = 1;
        $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
// Check if file already exists
        if (file_exists($target_file)) {
            $uploadOk = 0;
        }

        if ($uploadOk == 0) {
            echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
                $title = $_POST['title'];
                $answer = $_POST['answer'];
                $description = $_POST['description'];
                $url = "../admin/admin.php" . $_POST['current-url'];
                //insert customer register into database
                $sql = "UPDATE `exercise` SET `title` = '', `answer` = '', `description` = '' WHERE `exercise`.`id` = 5;";
                if ($conn->query($sql) === TRUE) {
                    header("Location:" . $url);
                } else {
                    echo "Sorry, there was an error uploading your file.";
                }
                $conn->close();
            } else {
                echo "Sorry, there was an error uploading your file.";
            }
        }
    }else{
        $title = $_POST['title'];
        $answer = $_POST['answer'];
        $description = $_POST['description'];
        $url = "../admin/admin.php" . $_POST['current-url-edit'];
        $id = $_POST['id-edit'];
        //insert customer register into database
        $sql = "UPDATE `exercise` SET `title` = '".$title."', `answer` = '".$answer."', `description` = '".$description."' WHERE `exercise`.`id` = '".$id."';";
        if ($conn->query($sql) === TRUE) {
            header("Location:" . $url);
        } else {
            echo "Sorry, there was an error uploading your file.";
        }
        $conn->close();
    }
}

?>