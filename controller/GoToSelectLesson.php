<?php
/**
 * Created by PhpStorm.
 * User: ducanh
 * Date: 06/12/2018
 * Time: 22:58
 */
include_once '../database/db.inc.php';
$lessons = [];
$search = $_POST['search'];

$query = "Select lesson_name,content from lessons ";
$result = $conn->query($query);
$numberOfRound = $result->num_rows;

if ($numberOfRound > 0) {
    while($row = $result->fetch_assoc()) {
        $row['content'] = utf8_encode($row['content']);
        array_push($lessons, $row);
    }
}

foreach ($lessons as $lesson) {
    if ($search == $lesson['lesson_name'] || $search == $lesson['content']) {
        $lessonNameArray = explode(" ", $lesson['lesson_name']);
        $lessonNumber = $lessonNameArray[1];
    }
}
echo $lessonNumber;